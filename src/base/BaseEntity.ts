import express from "express";
import { validateData } from "../utils/validation";
import { modelController } from "./BaseController";
import { Model, QueryOptions } from "mongoose";
import { formatErrors, returnData, returnError } from "./BaseFunctions";

export class BaseEntity {
  public path: string = "";
  public router = express.Router();
  public validationRules;
  public modelDTO;
  public model;
  public modelController = modelController;

  constructor(
    path: string,
    model: Model<any>,
    validationRules: any,
    modelDTO: any
  ) {
    this.path = path;
    this.model = model;
    this.validationRules = validationRules;
    this.modelDTO = modelDTO;
  }

  getAllModel = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const dataJSON = await this.modelController.getAllData(this.model);
      !dataJSON && this.throwDataNotFound();
      const data = dataJSON.map((v: any) => this.modelDTO.sender(v));
      this.returnData(response, data, "Data returned successfully");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };
  getAllUnRemoved = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const dataJSON = await this.modelController.getAllUnRemovedData(
        this.model
      );
      !dataJSON && this.throwDataNotFound();
      const data = dataJSON.map((v: any) => this.modelDTO.sender(v));
      this.returnData(response, data, "Data returned successfully");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };

  getAllModelNames = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const dataJSON = await this.modelController.getNames(this.model);
      !dataJSON && this.throwDataNotFound();
      this.returnData(response, dataJSON, "Data returned successfully");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };

  getModelByName = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const dataJSON = await this.modelController.getDataByName(
        this.model,
        request.params.name
      );
      !dataJSON && this.throwDataNotFound();
      const data = this.modelDTO.sender(dataJSON);
      this.returnData(response, data, "Data returned successfully.");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };

  getModelById = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const dataJSON = await this.modelController.getDataById(
        this.model,
        request.params.id
      );
      !dataJSON && this.throwDataNotFound();
      const data = this.modelDTO.sender(dataJSON);
      this.returnData(response, data, "Data returned successfully");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };

  createModel = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const modelData = request.body;
      const isValidated = validateData(this.validationRules, modelData);
      if (isValidated === "") {
        const modelJSONCreate = this.modelDTO.receiver(modelData);

        let data = await this.modelController.createData(
          this.model,
          modelJSONCreate
        );

        data = this.modelDTO.sender(data);
        return returnData(response, data, "Data created successfully", 201);
      }
      const { errors, message } = formatErrors(isValidated);
      returnError(response, message, errors);
    } catch (error: any) {
      const { errors, message } = formatErrors(error.message);
      returnError(response, message, errors);
    }
  };

  deleteModel = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const result = await this.modelController.deleteData(
        this.model,
        request.params.id
      );
      if (!result) {
        throw new Error("Not deleted");
      }
      this.returnData(response, "no-content", "Data deleted successfully.");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };
  softDeleteModel = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const result = await this.modelController.softDeleteData(
        this.model,
        request.params.id
      );
      if (!result) {
        throw new Error("Not deleted");
      }
      this.returnData(response, "no-content", "Data deleted successfully.");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };
  updateModel = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const modelData = request.body;
      const isValidated = validateData(this.validationRules, modelData);
      if (isValidated === "") {
        const modelJSONCreate = this.modelDTO.receiver(modelData);
        let data = await this.modelController.updateData(
          this.model,
          request.params.id,
          modelJSONCreate
        );
        if (!data) {
          return this.returnError(response, "Data not updated");
        }
        data = this.modelDTO.sender(data);
        return this.returnData(response, data, "Data updated successfully.");
      }
      const { errors, message } = formatErrors(isValidated);
      returnError(response, message, errors);
    } catch (error: any) {
      const { errors, message } = formatErrors(error.message);
      returnError(response, message, errors);
    }
  };

  getFirstDocument = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const dataJSON = await this.modelController.getFirstDocument(this.model);
      !dataJSON && this.throwDataNotFound();
      const data = this.modelDTO.sender(dataJSON);
      this.returnData(response, data, "Data returned successfully");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };

  getLastDocument = async (
    request: express.Request,
    response: express.Response
  ) => {
    try {
      const dataJSON = await this.modelController.getLastDocument(this.model);
      !dataJSON && this.throwDataNotFound();
      const data = this.modelDTO.sender(dataJSON);
      this.returnData(response, data, "Data returned successfully");
    } catch (error: any) {
      const { errors, message } = formatErrors(error.message);
      returnError(response, message, errors);
    }
  };

  getByQuery = async (
    request: express.Request,
    response: express.Response,
    query: Object,
    options: QueryOptions = { lean: true }
  ) => {
    try {
      const dataJSON = await this.modelController.getByQuery(
        this.model,
        query,
        options
      );
      !dataJSON && this.throwDataNotFound();
      const data = this.modelDTO.sender(dataJSON);
      this.returnData(response, data, "Data returned successfully");
    } catch (error: any) {
      const { errors, message } = formatErrors(error.message);
      returnError(response, message, errors);
    }
  };

  getAllByQuery = async (
    request: express.Request,
    response: express.Response,
    query: Object
  ) => {
    try {
      const dataJSON = await this.modelController.getAllByQuery(
        this.model,
        query
      );
      const data = dataJSON.map((v: any) => this.modelDTO.sender(v));
      this.returnData(response, data, "Data returned successfully");
    } catch (error: any) {
      const { errors, message } = formatErrors(error.message);
      returnError(response, message, errors);
    }
  };

  updateByQuery = async (
    request: express.Request,
    response: express.Response,
    filter: Object,
    options: QueryOptions = {
      lean: true,
      new: true,
    }
  ) => {
    try {
      const modelData = request.body;
      const isValidated = validateData(this.validationRules, request.body);
      if (isValidated === "") {
        let modelJSONCreate = this.modelDTO.receiver(modelData);
        let data = await this.modelController.updateByQuery(
          this.model,
          filter,
          modelJSONCreate,
          options
        );

        if (!data) {
          throw new Error("Data not updated. Try again");
        }
        data = this.modelDTO.sender(data);
        return this.returnData(response, data, "Updated successfully");
      }
      throw new Error(isValidated);
    } catch (error: any) {
      const { errors, message } = formatErrors(error.message);
      returnError(response, message, errors);
    }
  };

  deleteByQuery = async (
    request: express.Request,
    response: express.Response,
    query: Object,
    options: QueryOptions = { lean: true }
  ) => {
    try {
      const result = await this.modelController.deleteOneByQuery(
        this.model,
        query,
        options
      );
      if (!result) {
        throw new Error("Data not deleted");
      }
      this.returnData(response, "no-content", "Data deleted successfully.");
    } catch (error: any) {
      this.returnError(response, error.message);
    }
  };
  validateFields = (validationRules: any, modelData: any) => {
    try {
      const isValidated = validateData(validationRules, modelData);
      if (isValidated === "") {
        const modelJSONCreate = this.modelDTO.receiver(modelData);

        return { modelJSONCreate, message: "" };
      }
      return { modelJSONCreate: null, message: isValidated };
    } catch (error: any) {
      return { modelJSONCreate: null, message: error.message };
    }
  };

  validateFieldsCustomModel = (
    modelDTO: any,
    validationRules: any,
    modelData: any
  ) => {
    try {
      const isValidated = validateData(validationRules, modelData);
      if (isValidated === "") {
        const modelJSONCreate = modelDTO.receiver(modelData);

        return { modelJSONCreate, message: "" };
      }
      return { modelJSONCreate: null, message: isValidated };
    } catch (error: any) {
      return { modelJSONCreate: null, message: error.message };
    }
  };

  throwDataNotFound = () => {
    throw new Error("Data not found.");
  };

  returnError = (response: express.Response, message: string) => {
    response.status(400).json({
      status: 400,
      message: message,
    });
  };

  returnData = (
    response: express.Response,
    data: any,
    message: string = "Data returned successfully"
  ) => {
    response.status(200).json({
      status: 200,
      data,
      message,
    });
  };
}
