import { Model, QueryOptions } from "mongoose";

export const modelController = {
  getAllData: (model: any) => {
    return model.find({});
  },
  getAllUnRemovedData: (model: any) => {
    return model.find({ deleteState: false });
  },

  createData: async (model: any, data: any) => {
    return model.create(data);
  },
  deleteData: (model: any, id: any) => {
    return model.findOneAndDelete({ _id: id });
  },
  softDeleteData: (model: any, id: any) => {
    return model.findByIdAndUpdate(id, { deleteState: true });
  },
  getDataById: (model: any, id: String) => {
    return model.findOne({ _id: id });
  },
  updateData: (model: any, id: String, data: any) => {
    return model.findOneAndUpdate({ _id: id }, data, { new: true });
  },
  getDataByName: (model: any, name: String) => {
    return model.findOne({ name: name });
  },
  getNames: async (model: any) => {
    return await model.find({}, "name");
  },
  getFirstDocument: (model: any) => {
    return model.findOne({});
  },
  getLastDocument: (model: any) => {
    return model.findOne({}).sort({ createdAt: -1 });
  },
  getFirstDocumentByQuery: (model: any, query: any) => {
    return model.findOne(query);
  },
  getLastDocumentByQuery: (model: any, query: any) => {
    return model.findOne(query).sort({ createdAt: -1 });
  },
  createByQuery: (model: any, query: Object, options: QueryOptions) => {
    model = new model(query);
    return model.save(options);
  },
  getByQuery: (model: Model<any>, query: any, options: QueryOptions) => {
    return model.findOne(query, null, options);
  },
  getAllByQuery: (model: any, query: any) => {
    return model.find(query);
  },
  count: (model: any) => {
    return model.find({}).countDocuments();
  },
  countByQuery: (model: any, query: any) => {
    return model.find(query).countDocuments();
  },
  updateByQuery: async (
    model: any,
    filter: Object,
    query: Object,
    options: QueryOptions
  ) => {
    return model.findOneAndUpdate(filter, query, options);
  },
  deleteOneByQuery: (model: any, query: Object, options: QueryOptions) => {
    return model.deleteOne(query, options);
  },
  updatePasswordOrTransactionPassword: async (
    model: any,
    filter: any,
    query: any
  ) => {
    model = await model.findOne(filter);
    if (query.password) {
      model.password = query.password;
    }
    if (query.transactionPassword) {
      model.transactionPassword = query.transactionPassword;
    }
    await model.save();
    return true;
  },
};
