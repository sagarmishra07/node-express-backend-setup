export interface IBaseInterface {
  _id: String;
  createdAt: Date;
  updatedAt: Date;
}
export interface IReturnMessage {
  message: string;
  data: any;
  isDone: boolean;
  status: number;
}
export const iReturnMessage: IReturnMessage = {
  message: "",
  data: {},
  isDone: true,
  status: 0,
};
