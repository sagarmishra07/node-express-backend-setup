import express from "express";
import mongoose from "mongoose";
import bodyParser, { json } from "body-parser";
import { openRoutes } from "./modules";
import { db } from "./config/config";
require("dotenv").config();
const PORT = process.env.PORT || 8181;
const app = express();
app.use(json());

app.listen(PORT, () => {
  console.log("Server is listening on port 8181");
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  //doesn't send response just adjusts it
  res.header("Access-Control-Allow-Origin", "*"); //* to give access to any origin
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization" //to give access to all the headers provided
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET"); //to give access to all the methods provided
    return res.status(200).json({});
  }
  app.use("/", (req, res) => {
    res.send("api working...");
  });
  next(); //so that other routes can take over
});

Object.values(openRoutes).map((route) => app.use("/api", route));

mongoose
  .connect(db)
  .then((_) => {
    console.log("Connected to DB");
    console.log(".");
    console.log(".");
    console.log(".");
  })
  .catch((err) => {
    console.log(err);
  });
