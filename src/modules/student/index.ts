import { BaseEntity } from "../../base/BaseEntity";
import Schema from "./schema";
import { rules } from "./validator";
import { StudentDTO } from "./dto";
import { getStudents } from "./controller";
import { addStudents } from "./controller";
import { updateStudent } from "./controller";
import { deleteStudent } from "./controller";
export class Student extends BaseEntity {
  // Initializes the routes.
  constructor() {
    super("/student", Schema, rules, StudentDTO);
    this.initializeRoutes();
  }
  // Initialize routes.
  private initializeRoutes() {
    this.router.get(this.path, this.getAllModel);
    this.router.get(this.path, getStudents);
    this.router.post(this.path, addStudents);
    this.router.put(this.path + "/:id", updateStudent);
    this.router.delete(this.path + "/:id", deleteStudent);

    // this.router.post(this.path, this.createModel);
  }
}
