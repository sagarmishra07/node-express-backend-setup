import * as mongoose from "mongoose";

// Creates a mongoose schema with a list of students.

const students = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },

    phoneNumber: {
      type: String,
      required: [true, "Phone Number is required"],
      max: [10, "Phone number is not valid "],
      min: [8, "Phone number is not valid"],
      unique: [true, "Phone number Should be unique"],
    },

    address: {
      type: String,
      required: [true, "Address is required"],
    },
  },
  { timestamps: true }
);
export default mongoose.model<any>("Student", students);
