// export const rules for a phone number
export const rules = {
  name: { required: true, label: "Name" },

  phoneNumber: { required: true, label: "Phone number" },
  address: { required: true, label: "address" },
};
