import express, { request } from "express";
//BASE
import { returnData, returnError } from "../../base/BaseFunctions";
import { StudentDTO } from "./dto";
//Schema
import schema from "./schema";

// Fetch data from schema.
export const getStudents = async (
  request: express.Request,
  response: express.Response
) => {
  try {
    const data: any = await schema.find();
    if (!data) throw data;
    return returnData(response, data, "Data Fetched");
  } catch (error: any) {
    return returnError(response, error, error?.message || "Could not get data");
  }
};

// Adds a new student.
export const addStudents = async (
  request: express.Request,
  response: express.Response
) => {
  try {
    const data: any = StudentDTO.receiver(request.body);

    const storeStudent: any = await schema.create([data]);
    if (!storeStudent) throw storeStudent;

    return returnData(response, storeStudent, "Data Added");
  } catch (error: any) {
    return returnError(response, error, error?.message || "Could not add data");
  }
};
// Delete a student
export const deleteStudent = async (
  request: express.Request,
  response: express.Response
) => {
  try {
    const student_id = request.params.id;
    const data = await schema.findByIdAndDelete(student_id);
    if (!data) throw data;

    return returnData(response, data, "Data Deleted");
  } catch (error: any) {
    return returnError(
      response,
      error,
      error?.message || "Could not Delete data"
    );
  }
};
// Update student data.

export const updateStudent = async (
  request: express.Request,
  response: express.Response
) => {
  try {
    const student_id: string = request.params.id;
    const MODIFIED_JSON: any = StudentDTO.receiver(request.body);
    await schema.findOneAndUpdate(
      {
        _id: student_id,
      },
      MODIFIED_JSON
    );

    if (!MODIFIED_JSON) throw MODIFIED_JSON;

    return returnData(response, MODIFIED_JSON, "Data Updated");
  } catch (error: any) {
    return returnError(
      response,
      error,
      error?.message || "Could not update data"
    );
  }
};
