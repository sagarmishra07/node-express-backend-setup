import { IBaseInterface } from "../../base/BaseInterface";

// Export interface IStudent to IBaseInterface
export interface IStudent extends IBaseInterface {
  name: string;

  phoneNumber: string;
  address: string;
}

export const StudentDTO = {
  // Returns a dict with name phone number and address.
  receiver: (data: IStudent) => {
    return {
      name: data.name,

      phoneNumber: data.phoneNumber,
      address: data.address,
    };
  },
  // Returns the id name phoneNumber and createdAt values.
  sender: (data: IStudent) => {
    return {
      id: data._id,
      name: data.name,

      phoneNumber: data.phoneNumber,
      address: data.address,

      createdAt: data.createdAt,
    };
  },
};
